# RELEASE NOTES: **miscSymb**, to display miscellaneous symbols and pictographs on Linux.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.0.12**:
  - Updated build system components.

- **Version 1.0.11**:
  - Updated build system.

- **Version 1.0.10**:
  - Removed unused files.

- **Version 1.0.9**:
  - Updated build system component(s)

- **Version 1.0.8**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.0.7**:
  - Some minor changes in .comment file(s).

- **Version 1.0.6**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.0.5**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.0.4:**
  - Added *git push* before *git push --tags*.

- **Version 1.0.3:**
  - Added tagging of new release.

- **Version 1.0.2:**
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG, LICENSE and RELEASENOTES) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.0.1:**
  - Fixed typos in README.md file.

- **Version 1.0.0:**
  - First release.
