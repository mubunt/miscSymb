//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: miscSymb
// Description:  Display of Miscellaneous Symbols and Pictographs on Linux.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
//------------------------------------------------------------------------------
// ENUMS DEFINITIONS
//------------------------------------------------------------------------------
enum boolean { FALSE, TRUE };
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define a_4_character_string	TRUE
#define a_3_character_string	FALSE
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void _header(enum boolean n, const char *title) {
	if (n) {
		fprintf(stdout, "\n              %s\n", title);
		fprintf(stdout, "              ┌");
	} else {
		fprintf(stdout, "\n           %s\n", title);
		fprintf(stdout, "           ┌");
	}
	for (int k =0; k < 15; k++) fprintf(stdout, "────┬");
	fprintf(stdout, "────┐\n");
	if (n) fprintf(stdout, "  Codes       │");
	else  fprintf(stdout, "  Codes    │");
	for (int k =0; k < 16; k++) fprintf(stdout, " %1x  │", k);
	fprintf(stdout, "\n");
	if (n) fprintf(stdout, "┌─────────────┼");
	else fprintf(stdout, "┌──────────┼");
	for (int k =0; k < 15; k++) fprintf(stdout, "────┼");
	fprintf(stdout, "────┤\n");
}
static void _separator(enum boolean n) {
	if (n) fprintf(stdout, "├─────────────┼");
	else fprintf(stdout, "├──────────┼");
	for (int k =0; k < 15; k++) fprintf(stdout, "────┼");
	fprintf(stdout, "────┤\n");
}
static void _trailer(enum boolean n) {
	if (n) fprintf(stdout, "└─────────────┴");
	else fprintf(stdout, "└──────────┴");
	for (int k =0; k < 15; k++) fprintf(stdout, "────┴");
	fprintf(stdout, "────┘\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _codeSymbols1(unsigned char *s, unsigned int start, unsigned int stop, unsigned int incr) {
	for (unsigned int j = start; j <= stop; j += incr) {
		s[3] = (unsigned char) j;
		fprintf(stdout, "│ ");
		for (int k =0; k < 3; k++) fprintf(stdout, "%02x ", s[k] & 0xff);
		fprintf(stdout, "%1xx │", ((s[3] >> 4) & 0xf));
		for (unsigned int i = 0; i < 16; i++) {
			s[3] = (unsigned char) (j + i);
			enum boolean n = FALSE;
			switch (s[2]) {
			case (unsigned char) 0x8c:
				if ((s[3] >= (unsigned char) 0xa1 && s[3] <= (unsigned char) 0xac) || s[3] == (unsigned char) 0xb6)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x8d:
				if (s[3] == (unsigned char) 0xbd)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x8e:
				if (s[3] >= (unsigned char) 0x94 && s[3] <= (unsigned char) 0x9f)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x8f:
				if (( s[3] >= (unsigned char) 0x8b && s[3] <= (unsigned char) 0x8e)
				        || (s[3] >= (unsigned char) 0x94 && s[3] <= (unsigned char) 0x9f)
				        || (s[3] >= (unsigned char) 0xb1 && s[3] <= (unsigned char) 0xb3)
				        || ((s[3] >= (unsigned char) 0xb5 && s[3] <= (unsigned char) 0xb7)))
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x90:
				if (s[3] == (unsigned char) 0xbf)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x91:
				if (s[3] == (unsigned char) 0x81)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x93:
				if (s[3] >= (unsigned char) 0xbd && s[3] <= (unsigned char) 0xbe)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x94:
				if (s[3] >= (unsigned char) 0xbe && s[3] <= (unsigned char) 0xbf)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x95:
				if ((s[3] >= (unsigned char) 0x80 && s[3] <= (unsigned char) 0x8a)
				        || (s[3] == (unsigned char) 0x8f)
				        || (s[3] >= (unsigned char) 0xa8 && s[3] <= (unsigned char) 0xaf)
				        || (s[3] >= (unsigned char) 0xb0 && s[3] <= (unsigned char) 0xb9)
				        || (s[3] >= (unsigned char) 0xbb && s[3] <= (unsigned char) 0xbf))
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x96:
				if ((s[3] >= (unsigned char) 0x80 && s[3] <= (unsigned char) 0x94)
				        || (s[3] >= (unsigned char) 0x97 && s[3] <= (unsigned char) 0xa3)
				        || (s[3] >= (unsigned char) 0xa5 && s[3] <= (unsigned char) 0xbf))
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0x97:
				if (s[3] >= (unsigned char) 0x80 && s[3] <= (unsigned char) 0xba)
					fprintf(stdout, " %s  │", s);
				else fprintf(stdout, " %s │", s);
				break;
			case (unsigned char) 0xa0:
				if (s[3] >= (unsigned char) 0x8c && s[3] <= (unsigned char) 0x8f)
					fprintf(stdout, "    │");
				else fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xa1:
				if (( s[3] >= (unsigned char) 0x88 && s[3] <= (unsigned char) 0x8f)
				        || (s[3] >= (unsigned char) 0x9a && s[3] <= (unsigned char) 0x9f))
					fprintf(stdout, "    │");
				else fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xa2:
				if (( s[3] >= (unsigned char) 0x88 && s[3] <= (unsigned char) 0x8f)
				        || (s[3] >= (unsigned char) 0xae && s[3] <= (unsigned char) 0xaf)
				        || (s[3] >= (unsigned char) 0xb0 && s[3] <= (unsigned char) 0xbf))
					fprintf(stdout, "    │");
				else fprintf(stdout, " %s  │", s);
				break;
			default:
				fprintf(stdout, " %s │", s);
				break;
			}
		}
		fprintf(stdout, "\n");
	}
}

static void _codeSymbols2(unsigned char *s, unsigned int start, unsigned int stop, unsigned int incr) {
	for (unsigned int j = start; j <= stop; j += incr) {
		s[2] = (unsigned char) j;
		fprintf(stdout, "│ ");
		for (int k =0; k < 2; k++) fprintf(stdout, "%02x ", s[k] & 0xff);
		fprintf(stdout, "%1xx │", ((s[2] >> 4) & 0xf));
		for (unsigned int i = 0; i < 16; i++) {
			s[2] = (unsigned char) (j + i);
			switch (s[1]) {
			case (unsigned char) 0x9c:
				if (s[2] == (unsigned char) 0x85 || s[2] == (unsigned char) 0x8a || s[2] == (unsigned char) 0x8b || s[2] == (unsigned char) 0xa8)
					fprintf(stdout, " %s │", s);
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0x9d:
				if ((s[2] == (unsigned char) 0x8c) || (s[2] == (unsigned char) 0x8e)
				        || (s[2] >= (unsigned char) 0x93 && s[2] <= (unsigned char) 0x95)
				        || (s[2] == (unsigned char) 0x97))
					fprintf(stdout, " %s │", s);
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0x9e:
				if ((s[2] >= (unsigned char) 0x95 && s[2] <= (unsigned char) 0x97)
				        || (s[2] == (unsigned char) 0xb0) || (s[2] == (unsigned char) 0xbf))
					fprintf(stdout, " %s │", s);
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xac:
				if (s[2] >= (unsigned char) 0x9b && s[2] <= (unsigned char) 0x9c)
					fprintf(stdout, " %s │", s);
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xad:
				if ((s[2] == (unsigned char) 0x90) || (s[2] == (unsigned char) 0x95))
					fprintf(stdout, " %s │", s);
				else if ((s[2] == (unsigned char) 0xb4) || (s[2] == (unsigned char) 0xb5))
					fprintf(stdout, "    │");
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xae:
				if ((s[2] >= (unsigned char) 0x96 && s[2] <= (unsigned char) 0x97)
				        || (s[2] >= (unsigned char) 0xba && s[2] <= (unsigned char) 0xbc))
					fprintf(stdout, "    │");
				else
					fprintf(stdout, " %s  │", s);
				break;
			case (unsigned char) 0xaf:
				if ((s[2] == (unsigned char) 0x89)
				        || (s[2] >= (unsigned char) 0x93 && s[2] <= (unsigned char) 0x9f)
				        || (s[2] >= (unsigned char) 0xa0 && s[2] <= (unsigned char) 0xab)
				        || (s[2] >= (unsigned char) 0xb0 && s[2] <= (unsigned char) 0xbf))
					fprintf(stdout, "    │");
				else
					fprintf(stdout, " %s  │", s);
				break;
			default:
				fprintf(stdout, " %s  │", s);
				break;
			}
		}
		fprintf(stdout, "\n");
	}
}

static void _codeSymbols3(unsigned char *s) {
	fprintf(stdout, "│ ");
	for (int k =0; k < 2; k++) fprintf(stdout, "%02x ", s[k] & 0xff);
	fprintf(stdout, "%1xx │", ((s[2] >> 4) & 0xf));
	unsigned int k = (unsigned int) s[2];
	for (unsigned int i = 0; i < 16; i++) {
		s[2] = (unsigned char) (k + i);
		fprintf(stdout, " %s  │", s);
	}
	fprintf(stdout, "\n");
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main( void ) {
	unsigned char code[5];
	//--------------------------------------------------------------------------
	_header(a_4_character_string, "MISCELLANEOUS SYMBOLS AND PICTOGRAPHS");
	code[0] = 0xf0;
	code[1] = 0x9f;
	code[4] = 0;
	for (unsigned int i = 0x8c; i <= 0x97; i++) {
		code[2] = (unsigned char) i;
		code[3] = 0;
		_codeSymbols1(code, 0x80, 0xbf, 0x10);
		if (i != 0x97) _separator(a_4_character_string);
	}
	_trailer(a_4_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "ARROWS");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0x86; i <= 0x87; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		if (i == 0x86) {
			_codeSymbols2(code, 0x90, 0xbf, 0x10);
			_separator(a_3_character_string);
		} else {
			_codeSymbols2(code, 0x80, 0xbf, 0x10);
		}
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "SUPPLEMENTAL ARROWS-A");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0x9f; i <= 0x9f; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		_codeSymbols2(code, 0xb0, 0xbf, 0x10);
		if (i != 0x9f) _separator(a_3_character_string);
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "SUPPLEMENTAL ARROWS-B");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0xa4; i <= 0xa5; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		_codeSymbols2(code, 0x80, 0xb0, 0x10);
		if (i != 0xa5) _separator(a_3_character_string);
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_4_character_string, "SUPPLEMENTAL ARROWS-C");
	code[0] = 0xf0;
	code[1] = 0x9f;
	code[4] = 0;
	for (unsigned int i = 0xa0; i <= 0xa2; i++) {
		code[2] = (unsigned char) i;
		code[3] = 0;
		_codeSymbols1(code, 0x80, 0xbf, 0x10);
		if (i != 0xa2) _separator(a_4_character_string);
	}
	_trailer(a_4_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "MISCELLANEOUS SYMBOLS AND ARROWS");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0xac; i <= 0xaf; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		_codeSymbols2(code, 0x80, 0xbf, 0x10);
		if (i != 0xaf) _separator(a_3_character_string);
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "DINGBATS");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0x9c; i <= 0x9e; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		_codeSymbols2(code, 0x80, 0xbf, 0x10);
		if (i != 0x9e) _separator(a_3_character_string);
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "BLOCK ELEMENTS");
	code[0] = 0xe2;
	code[3] = 0;
	code[1] = (unsigned char) 0x96;
	code[2] = 0;
	_codeSymbols2(code, 0x80, 0x8f, 0x10);
	_separator(a_3_character_string);
	_codeSymbols2(code, 0x90, 0x9f, 0x10);
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	_header(a_3_character_string, "BOX-DRAWING CHARACTERS");
	code[0] = 0xe2;
	code[3] = 0;
	for (unsigned int i = 0x94; i <= 0x95; i++) {
		code[1] = (unsigned char) i;
		code[2] = 0;
		for (unsigned int j = 0x80; j <= 0xbf; j += 0x10) {
			code[2] = (unsigned char) j;
			_codeSymbols3(code);
			if (i != 0x95 || j != 0xb0) _separator(a_3_character_string);
		}
	}
	_trailer(a_3_character_string);
	//--------------------------------------------------------------------------
	return EXIT_SUCCESS;
}