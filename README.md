# *miscSymb*, a program to display miscellaneous symbols and pictographs on Linux.

Program not very interesting except that it allows to have, in one place and for Linux, the values of these miscellaneous symbols and pictographs, plus arrows and other specific characters.

#### References
- [Miscellaneous Symbols and Pictographs](https://en.wikipedia.org/wiki/Miscellaneous_Symbols_and_Pictographs)
- [Arrow (symbol)](https://en.wikipedia.org/wiki/Arrow_(symbol)#Arrows_in_Unicode)
- [Block Elements](https://en.wikipedia.org/wiki/Block_Elements)
- [Box-drawing character](https://en.wikipedia.org/wiki/Box-drawing_character)

## LICENSE
**miscSymb** is covered by the GNU General Public License (GPL) version 3 and above.

## OUTPUT
![miscSymb](README_images/1.png  "Output")
![miscSymb](README_images/2.png  "Output")
![miscSymb](README_images/3.png  "Output")
![miscSymb](README_images/4.png  "Output")

## STRUCTURE OF THE APPLICATION
This section walks you through **miscSymb**'s structure. Once you understand this structure, you will easily find your way around in **miscSymb**'s code base.
``` bash
$ yaTree
./                  # Application level
├── README_images/  # Images for documentation
│   ├── 1.png       # 
│   ├── 2.png       # 
│   ├── 3.png       # 
│   └── 4.png       # 
├── src/            # Source directory
│   ├── Makefile    # Makefile
│   └── miscsymb.c  # The program
├── COPYING.md      # GNU General Public License markdown file
├── LICENSE.md      # License markdown file
├── Makefile        # Makefile
├── README.md       # ReadMe Mark-Down file
├── RELEASENOTES.md # Release Notes markdown file
└── VERSION         # Version identification text file

2 directories, 12 files
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd miscSymb
$ make clean all
$ ./linux/miscsymb
```
## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd miscSymb
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```
## NOTES
- Application woks fine with *terminator* and *tilix*, and NOT with *xterm*, *uxtem*.
- Application developed and tested with gcc 7.3.0 running on XUBUNTU 18.04.

## SOFTWARE REQUIREMENTS
- For development and usage: Nothing particular...

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .
***
